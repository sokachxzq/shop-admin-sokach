import { defineStore } from 'pinia'
import { ref } from 'vue'
import { getinfoApi } from '@/api/manager'
import { removeToken } from '@/utils/cookie'
import { useRouter } from 'vue-router'

const useUserStore = defineStore('user', () => {
  const router = useRouter()

  //用户信息
  const user = ref({})
  //菜单数据
  const menus = ref([])
  //权限数据
  const ruleNames = ref([])
  //菜单展开及收缩
  const isMenu = ref(false)

  //修改用户信息
  const setUser = (val) => {
    user.value = val
  }

  //获取当前用户信息
  const getUserInfo = async () => {
    const res = await getinfoApi()
    user.value = res
    menus.value = res.menus
    ruleNames.value = res.ruleNames
  }

  //退出当前登录用户
  const logout = () => {
    removeToken()
    user.value = {}
    router.push('/login')
  }

  //修改菜单收缩或展开
  const setIsMenu = () => {
    isMenu.value = !isMenu.value
  }

  return { user, setUser, getUserInfo, logout, setIsMenu, isMenu, menus, ruleNames }
})

export default useUserStore
