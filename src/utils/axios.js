import axios from 'axios'
import { getToken } from './cookie'
import { ElNotification } from 'element-plus'
import useUserStore from '@/store/user'

const service = axios.create({
  baseURL: '/api',
  timeout: 3000
})

// 添加请求拦截器
service.interceptors.request.use(
  function (config) {
    // 往header头自动添加token
    const token = getToken()
    if (token) {
      config.headers['token'] = token
    }

    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
service.interceptors.response.use(
  function (response) {
    return response.request.responseType == 'blob' ? response.data : response.data.data
  },
  function (error) {
    const msg = error.response.data.msg || '请求失败'

    if (msg == '非法token，请先登录！') {
      const userStore = useUserStore()
      userStore.logout().finally(() => location.reload())
    }

    toast(msg, 'error')

    return Promise.reject(error)
  }
)

export default service
