import { ElNotification } from 'element-plus'

export const toast = (message, type = 'success') => {
  ElNotification({
    message,
    type,
    duration: 3000
  })
}
