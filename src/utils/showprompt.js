//弹出输入框
import { ElMessageBox } from 'element-plus'
export const showprompt = (tip, value = '') => {
  return ElMessageBox.prompt(tip, '', {
    confirmButtonText: '确认',
    cancelButtonText: '取消',
    inputValue: value
  })
}
