import service from '@/utils/axios'

//获取商品列表
export const getGoodsListApi = (page, params) => {
  return service.get(`/goods/${page}`, { params })
}

//获取分类列表
export const getCategoryApi = () => {
  return service.get('/category')
}

//审核商品
export const checkApi = (id, ischeck) => {
  return service.post(`/goods/${id}/check`, { ischeck })
}

//删除商品
export const deleteApi = (id) => {
  return service.post('/goods/delete_all', { ids: [id] })
}

//批量上下架商品
export const changestatusApi = (ids, status) => {
  return service.post('/goods/changestatus', { ids, status })
}

//批量删除
export const deleteAllApi = (ids) => {
  return service.post('/goods/delete_all', { ids })
}

//增加商品
export const createApi = (data) => {
  return service.post('/goods', data)
}

//修改商品
export const updateApi = (id, data) => {
  return service.post(`/goods/${id}`, data)
}

//恢复商品
export const restoreApi = (ids) => {
  return service.post(`/goods/restore`, { ids })
}

//彻底删除商品
export const destroyApi = (ids) => {
  return service.post(`/goods/destroy`, { ids })
}

//获取商品资料
export const getBannersApi = (id) => {
  return service.get(`/goods/read/${id}`)
}

//提交轮播图设置
export const setGoodsBannerApi = (id, data) => {
  return service.post(`/goods/banners/${id}`, data)
}
