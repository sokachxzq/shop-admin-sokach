import service from '@/utils/axios'

//获取分销数据统计
export const statisticsApi = () => {
  return service.get(`/agent/statistics`)
}
