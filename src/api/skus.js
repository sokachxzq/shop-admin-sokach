import service from '@/utils/axios'

//新增
export const createApi = (data) => {
  return service.post('/skus', data)
}

//修改
export const updateApi = (id, data) => {
  return service.post(`/skus/${id}`, data)
}

//删除
export const deleteApi = (id) => {
  return service.post(`/skus/delete_all`, { ids: [id] })
}

//删除全部
export const deletAllApi = (id) => {
  return service.post(`/skus/delete_all`, { ids: id })
}

//更新
export const updateStatusApi = (id, status) => {
  return service.post(`/skus/${id}/update_status`, { status })
}

//获取
export const getList = (page = 1) => {
  return service.get(`skus/${page}`)
}
