import service from '@/utils/axios'

//删除公告
export const deleteNoticeApi = (id) => {
  return service.post(`/notice/${id}/delete`)
}

//获取公告列表
export const getNoticeApi = (page = 1) => {
  return service.get(`/notice/${page}`)
}

//新增公告
export const addNoticeApi = (data) => {
  return service.post(`/notice`, data)
}

//修改公告
export const changeNoticeApi = (id, data) => {
  return service.post(`/notice/${id}`, data)
}
