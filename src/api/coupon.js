import service from '@/utils/axios'

//新增
export const createApi = (data) => {
  const { name, type, value, total, min_price, start_time, end_time, order, desc } = data
  return service.post('/coupon', {
    name,
    type,
    value,
    total,
    min_price,
    start_time: new Date(start_time).getTime(),
    end_time: new Date(end_time).getTime(),
    order,
    desc
  })
}

//修改
export const updateApi = (id, data) => {
  const { name, type, value, total, min_price, start_time, end_time, order, desc } = data
  return service.post(`/coupon/${id}`, {
    name,
    type,
    value,
    total,
    min_price,
    start_time: new Date(start_time).getTime(),
    end_time: new Date(end_time).getTime(),
    order,
    desc
  })
}

//删除
export const deleteApi = (id) => {
  return service.post(`/coupon/${id}/delete`)
}

//更新
export const updateStatusApi = (id, status) => {
  return service.post(`/coupon/${id}/update_status`, { status })
}

//获取
export const getList = (page = 1) => {
  return service.get(`coupon/${page}`)
}
