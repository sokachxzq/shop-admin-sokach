import service from '@/utils/axios'

//获取
export const getManagerList = (page, params) => {
  return service.get(`user/${page}`, { params })
}

//更新状态
export const updateStatusApi = (id, status) => {
  return service.post(`/user/${id}/update_status`, { status })
}

//修改
export const updateManagerInfoApi = (id, data) => {
  return service.post(`/user/${id}`, data)
}

//删除
export const deleteManagerByIdApi = (id) => {
  return service.post(`/user/${id}/delete`)
}

//新增
export const createManagerApi = (data) => {
  return service.post('/user', data)
}

//获取会员等级
export const getuserLevelApi = () => {
  return service.get(`user_level/1`)
}
