import service from '@/utils/axios'

//获取
export const getListApi = (page, params) => {
  return service.get(`goods_comment/${page}`, { params })
}

//更新
export const updateStatusApi = (id, status) => {
  return service.post(`/goods_comment/${id}/update_status`, { status })
}

//回复
export const replyApi = (id, data) => {
  return service.post(`/goods_comment/review/${id}`, { data })
}
