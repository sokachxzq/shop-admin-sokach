import service from '@/utils/axios'

//新增
export const createApi = (data) => {
  return service.post('/role', data)
}

//修改
export const updateApi = (id, data) => {
  return service.post(`/role/${id}`, data)
}

//删除
export const deleteApi = (id) => {
  return service.post(`/role/${id}/delete`)
}

//更新
export const updateStatusApi = (id, status) => {
  return service.post(`/role/${id}/update_status`, { status })
}

//获取
export const getList = (page = 1) => {
  return service.get(`role/${page}`)
}

//配置权限
export const setRules = (id, rule_ids) => {
  return service.post('/role/set_rules', {
    id,
    rule_ids
  })
}
