import service from '@/utils/axios'

//获取统计数据1
export const statistics1Api = () => {
  return service.get('/statistics1')
}

//获取统计数据3
export const statistics3Api = (type) => {
  return service.get('statistics3?type=' + type)
}

//获取统计数据2
export const statistics2Api = () => {
  return service.get('statistics2')
}
