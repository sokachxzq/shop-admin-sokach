import service from '@/utils/axios'

//获取商品列表
export const getGoodsListApi = (page, params) => {
  return service.get(`/order/${page}`, { params })
}

//批量删除
export const deleteAllApi = (ids) => {
  return service.post('/order/delete_all', { ids })
}

//订单发货
export const shipApi = (id, data) => {
  return service.post(`order/${id}/ship`, data)
}

//获取快递公司列表
export const companyApi = () => {
  return service.get(`express_company/1`)
}

//同意或拒绝退款
export const refundApi = (id, data) => {
  return service.post(`/order/${id}/handle_refund`, data)
}

//查看物流信息
export const shipinfoApi = (id) => {
  return service.get(`/order/${id}/get_ship_info`)
}

//导出订单信息 一定要加 responseType: 'blob'，否则文件会损坏
export const exportExcelApi = (tab, data) => {
  return service.post(`order/excelexport?tab=${tab}`, data, { responseType: 'blob' })
}
