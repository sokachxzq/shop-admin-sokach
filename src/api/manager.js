import service from '@/utils/axios'

//登录
export const loginApi = (username, password) => {
  return service.post('/login', { username, password })
}

//获取用户信息
export const getinfoApi = () => {
  return service.post('/getinfo')
}

//修改密码
export const updatePassword = (data) => {
  return service.post('updatepassword', data)
}

//获取管理员列表
export const getManagerList = (page, limit = null, keyword = null) => {
  return service.get(`manager/${page}?limit=${limit}&keyword=${keyword}`)
}

//更新管理员权限状态
export const updateStatusApi = (id, status) => {
  return service.post(`/manager/${id}/update_status`, { status })
}

//修改管理员信息
export const updateManagerInfoApi = (id, data) => {
  return service.post(`/manager/${id}`, data)
}

//删除管理员信息
export const deleteManagerByIdApi = (id) => {
  return service.post(`/manager/${id}/delete`)
}

//新增管理员信息
export const createManagerApi = (data) => {
  return service.post('/manager', data)
}
