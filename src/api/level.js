import service from '@/utils/axios'

//删除公告
export const deleteNoticeApi = (id) => {
  return service.post(`/user_level/${id}/delete`)
}

//获取公告列表
export const getNoticeApi = (page = 1) => {
  return service.get(`/user_level/${page}`)
}

//新增公告
export const addNoticeApi = (data) => {
  return service.post(`/user_level`, data)
}

//修改公告
export const changeNoticeApi = (id, data) => {
  return service.post(`/user_level/${id}`, data)
}

//更新状态
export const updateStatusApi = (id, status) => {
  return service.post(`/user_level/${id}/update_status`, { status })
}
