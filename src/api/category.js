import service from '@/utils/axios'

//获取
export const getListApi = () => {
  return service.get(`/category`)
}

//更新状态
export const changeStatusApi = (id, status) => {
  return service.post(`/category/${id}/update_status`, { status })
}

//新增
export const createApi = (name) => {
  return service.post(`/category`, { name })
}

//删除
export const deleteApi = (id) => {
  return service.post(`/category/${id}/delete`)
}

//修改
export const changeApi = (id, name) => {
  return service.post(`/category/${id}`, { name })
}

//获取关联列表
export const relevanceApi = (id) => {
  return service.get(`/app_category_item/list?category_id=${id}`)
}

//删除关联列表
export const deleteRelevanceApi = (id) => {
  return service.post(`/app_category_item/${id}/delete`)
}

//获取商品列表
export const getGoodsListApi = (page) => {
  return service.get(`/goods/${page}?tab=saling`)
}

//增加关联
export const addRelevanceApi = (category_id, goods_ids) => {
  return service.post(`/app_category_item`, { category_id, goods_ids })
}
