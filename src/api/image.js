import service from '@/utils/axios'

//获取统计数据1
export const imageApi = (page) => {
  return service.get('/image_class/' + page)
}

//增加图库分类
export const addImageApi = (data) => {
  return service.post('/image_class', data)
}

//修改图库分类
export const changeImageApi = (id, data) => {
  return service.post('/image_class/' + id, data)
}

//删除图片分类
export const deleteImageApi = (id) => {
  return service.post(`image_class/${id}/delete`)
}

//获取指定分类的图片列表
export const getImageByIdApi = (id, page = 1) => {
  return service.get(`image_class/${id}/image/${page}`)
}

//删除指定图片
export const deleteImageByIdApi = (ids) => {
  return service.post('/image/delete_all', { ids })
}

//重新命名图片
export const changeImageNameByIdApi = (id, name) => {
  return service.post(`image/${id}`, { name })
}

export const uploadIamgeAction = '/api/image/upload'
