import service from '@/utils/axios'

//新增菜单权限
export const createRuleApi = (data) => {
  return service.post('/rule', data)
}

//修改菜单权限
export const updateRuleInfoApi = (id, data) => {
  return service.post(`/rule/${id}`, data)
}

//删除菜单权限
export const deleteRuleByIdApi = (id) => {
  return service.post(`/rule/${id}/delete`)
}

//更新菜单权限状态
export const updateStatusApi = (id, status) => {
  return service.post(`/rule/${id}/update_status`, { status })
}

//获取菜单权限列表
export const getRuleList = (page = 1) => {
  return service.get(`rule/${page}`)
}
